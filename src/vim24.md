# 24 bit colors in terminal vim

I'm running the terminal emulator Alacritty and the multiplexer tmux.
Inside that, I'm running vim, which kept using 256 color mode.
This made changing color settings difficult, because nothing would work _quite_
right.

## Shell test

Before you can get 24 bit colors in vim, you need 24 bit colors in your
terminal.
Color, being what it is, can be notoriously difficult to know if it is right.
So the first thing we are going to do is run something to check this.

I used [this][gist] gist.
This is what the output _should_ look like:

![Working output](img/24bit-term.png)

This is an example of what a broken output could look like:

![Broken output](img/24bit-term-broken.png)

## Verify outer `TERM`

Open the terminal, without tmux, and check your `$TERM`.
It should match the terminal, `alacritty` in my case.
`xterm-256color` is also common.

Run the shell test to make sure 24 bit color is working.

## Verify inner `TERM`

Start tmux, and run check `$TERM`.
It should be `tmux-256color`.
Run the shell test to verify it is working.

If it is not, edit your tmux config to include:
```
set -g default-terminal "tmux-256color"
set -as terminal-features ",alacritty:RGB"
```

## Configure vim for 24 bit color

Include the following in your `.vimrc`:
```
" 24 bit color
if (has("termguicolors"))
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
```

My problem in particular was missing the `let &t_8...` lines.
When I set `termguicolors`, everything went monochrome.

[gist]: https://gist.github.com/grhbit/db6c5654fa976be33808b8b33a6eb861
