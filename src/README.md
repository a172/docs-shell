# Introduction

Just a collection of shell tricks and quirks to make the IT life easier.

---

[Source][src] for this document.
Large and small contributions welcome!


[src]: https://gitlab.com/a172/docs-shell
